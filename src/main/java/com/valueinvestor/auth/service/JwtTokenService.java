package com.valueinvestor.auth.service;

import javax.servlet.http.HttpServletResponse;

import com.valueinvestor.jpa.auth.User;

public interface JwtTokenService {
    
    String addAuthentication(User user, HttpServletResponse response);
}
