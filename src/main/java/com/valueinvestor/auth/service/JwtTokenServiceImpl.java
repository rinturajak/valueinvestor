package com.valueinvestor.auth.service;


import static com.valueinvestor.security.util.JwtTokenGenerator.signAndSerializeJWT;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.valueinvestor.jpa.auth.User;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {
    
    @Value("${jwt.header}")
    private String header;
    
    @Value("${jwt.secret}")
    private String secret;
    
    @Override
    public String addAuthentication(final User user, final HttpServletResponse response) {
        final String signAndSerializeJWT = signAndSerializeJWT(user, secret);
        response.addHeader(header, "Bearer " + signAndSerializeJWT);
        return signAndSerializeJWT;
    }
    
    
    

}
