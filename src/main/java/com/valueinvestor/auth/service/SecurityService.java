package com.valueinvestor.auth.service;

public interface SecurityService {
    String findLoggedInUsername();

    void generateAndSetUsernameAuthenticationToken(String username, String password);
}
