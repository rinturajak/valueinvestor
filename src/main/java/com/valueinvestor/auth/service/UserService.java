package com.valueinvestor.auth.service;

import com.valueinvestor.jpa.auth.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
