package com.valueinvestor.batch.company;

import java.util.Date;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.valueinvestor.batch.processor.CompanyItemProcessor;
import com.valueinvestor.batch.reader.CompanyReaderConfiguration;
import com.valueinvestor.batch.tasklet.CompanyCleanUpTasklet;
import com.valueinvestor.batch.tasklet.PopulateExcelMapperTasklet;
import com.valueinvestor.batch.tasklet.SortCompanyTableTasklet;
import com.valueinvestor.model.CompanySQLDto;
import com.valueinvestor.model.CompanyVO;

import lombok.extern.slf4j.Slf4j;

@Lazy
@Slf4j
@Component
public class CompanySyncJob {
    
    @Resource
    private DataSource dataSource;
    
    @Resource
    private JdbcTemplate jdbcTemplate;

    @Resource
    private SimpleJobLauncher jobLauncher;

    @Resource
    private JobBuilderFactory jobBuilderFactory;

    @Resource
    private StepBuilderFactory stepBuilderFactory;
    
    @Resource
    private CompanyReaderConfiguration companyReaderConfiguration;
    
    @Resource
    private CompanyItemProcessor companyItemProcessor;
    
    @Resource
    private CompanyWriter companyWriter;
    
    @Resource
    private CompanyCleanUpTasklet companyCleanUpTasklet;
    
    @Resource
    private SortCompanyTableTasklet sortCompanyTableTasklet;
    
    @Resource
    private PopulateExcelMapperTasklet populateExcelMapperTasklet;
    
    public void executeCompanySyncJob(final org.springframework.core.io.Resource resource) throws Exception {
        log.debug("Company Table Sync Job Started at :" + new Date());

        final JobParameters param = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis())).toJobParameters();
        final JobExecution execution = jobLauncher.run(syncCompanyJob(resource), param);

        log.debug("Company Table Sync Job finished with status :" + execution.getStatus());
    }
    
    public Job syncCompanyJob(final org.springframework.core.io.Resource resource) {
        return jobBuilderFactory.get("syncCompanyTable")
                .incrementer(new RunIdIncrementer())
                .start(cleanUpCompanyStep())
                .next(populateExcelMappingStep())
                .next(syncCompanyStep(resource))
                .next(sortCompanyTableStep())
                .build();
    }
    
    public Step cleanUpCompanyStep() {
      return stepBuilderFactory.get("cleanUpCompanyStep")
              .tasklet(companyCleanUpTasklet)
              .build();
    }
    
    public Step populateExcelMappingStep() {
      return stepBuilderFactory.get("populateExcelMappingStep")
              .tasklet(populateExcelMapperTasklet)
              .build();
    }
    
    public Step syncCompanyStep(final org.springframework.core.io.Resource resource) {
        return stepBuilderFactory.get("syncCompanyStep")
                .<CompanyVO, CompanySQLDto>chunk(1000)
                .reader(companyReaderConfiguration.companyReader(resource))
                .processor(companyItemProcessor)
                .writer(companyWriter)
                .build();
    }
    
    public Step sortCompanyTableStep() {
      return stepBuilderFactory.get("sortCompanyTableStep")
              .tasklet(sortCompanyTableTasklet)
              .build();
    }
    
    
}
