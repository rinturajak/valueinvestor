package com.valueinvestor.batch.company;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.valueinvestor.model.CompanySQLDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class CompanyWriter implements ItemWriter<CompanySQLDto> {
  
  @Resource
  private JdbcTemplate jdbcTemplate;

  @Override
  public void write(final List<? extends CompanySQLDto> companySQLDtos) throws Exception {
      for (final CompanySQLDto companySQLDto: companySQLDtos) {
        log.debug("Insert Statement from custom writer"+ companySQLDto.getInsertSql());
        jdbcTemplate.execute(companySQLDto.getInsertSql());
      }
  }

}
