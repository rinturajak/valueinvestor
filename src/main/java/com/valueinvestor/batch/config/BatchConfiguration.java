package com.valueinvestor.batch.config;

import java.util.Date;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;

import com.valueinvestor.batch.processor.BSEBhavCopyItemProcessor;
import com.valueinvestor.batch.processor.BhavCopyItemProcessor;
import com.valueinvestor.batch.processor.NSEBhavCopyItemProcessor;
import com.valueinvestor.batch.tasklet.SortCompanyTableTasklet;
import com.valueinvestor.model.BSEBhavCopyVO;
import com.valueinvestor.model.BhavCopyVO;
import com.valueinvestor.model.NSEBhavCopyVO;
import com.valueinvestor.reader.BhavCopyReader;
import com.valueinvestor.reader.BhavCopyReaderHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableBatchProcessing
@Import(BatchSchedulerConfiguration.class)
public class BatchConfiguration {
    
    @Resource
    private DataSource dataSource;

    @Resource
    private SimpleJobLauncher jobLauncher;

    @Resource
    private JobBuilderFactory jobBuilderFactory;

    @Resource
    private StepBuilderFactory stepBuilderFactory;
    
    @Resource
    private BhavCopyReaderHelper bhavCopyReaderHelper;
    
    @Resource
    private BhavCopyReader bhavCopyReader;

    @Resource
    private BSEBhavCopyItemProcessor bseBhavCopyItemProcessor;
    
    @Resource
    private NSEBhavCopyItemProcessor nseBhavCopyItemProcessor;
    
    @Resource
    private ItemReader<BhavCopyVO> bhavCopyItemReader;
    
    @Resource
    private BhavCopyItemProcessor bhavCopyItemProcessor;
    
    @Resource
    private SortCompanyTableTasklet sortCompanyTableTasklet;
    
    //@Scheduled(cron = "*0 0/10 * * * MON-FRI")
    @Scheduled(cron = "*0 0/10 * * * *")
    public void perform() throws Exception {
        //TODO: Change the milli time to Date
        log.debug("Job Started at :" + new Date());

        final JobParameters param = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis())).toJobParameters();
        final JobExecution execution = jobLauncher.run(syncBhavCopyJob(), param);

        log.debug("Job finished with status :" + execution.getStatus());
    }
    
    @Lazy
    @Bean
    public JdbcBatchItemWriter<BhavCopyVO> writer() {
        final JdbcBatchItemWriter<BhavCopyVO> writer = new JdbcBatchItemWriter<>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<BhavCopyVO>());
        writer.setSql("CALL bhav_copy_save(:code, :name, :closingPrice, :modifiedDate)");
        writer.setDataSource(dataSource);
        return writer;
    }
    
    @Lazy
    @Bean
    public JdbcBatchItemWriter<BhavCopyVO> updateCompanyWriter() {
        final JdbcBatchItemWriter<BhavCopyVO> updateCompanyWriter = new JdbcBatchItemWriter<>();
        updateCompanyWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<BhavCopyVO>());
        updateCompanyWriter.setSql("UPDATE company_table SET share_price = :closingPrice, modified_date = :modifiedDate WHERE bse_code = :code OR nse_symbol = :code"); 
        updateCompanyWriter.setDataSource(dataSource);
        updateCompanyWriter.afterPropertiesSet();
        return updateCompanyWriter;
    }
    
    @Lazy
    @Bean
    public Job syncBhavCopyJob() {
        return jobBuilderFactory.get("syncBhavCopy")
                .incrementer(new RunIdIncrementer())
                .start(syncBseBhavCopyStep())
                .next(syncNseBhavCopyStep())
                .next(updateCompanySharePriceStep())
                .next(sortCompanyTableStep())
                .build();
    }
    
    @Lazy
    @Bean
    public Step syncBseBhavCopyStep() {
        return stepBuilderFactory.get("syncBseBhavCopyStep")
                .<BSEBhavCopyVO, BhavCopyVO>chunk(1000)
                .reader(bseBhavCopyReader())
                .processor(bseBhavCopyItemProcessor)
                .writer(writer())
                .build();
    }
    
    private FlatFileItemReader<BSEBhavCopyVO> bseBhavCopyReader() {
      final FlatFileItemReader<BSEBhavCopyVO> reader = new FlatFileItemReader<>();
      reader.setResource(bhavCopyReader.reader(bhavCopyReaderHelper.getBseBhavCopyDownloadUrl()));
      reader.setLinesToSkip(1);
      reader.setLineMapper(new DefaultLineMapper<BSEBhavCopyVO>() {
          {
              setLineTokenizer(new DelimitedLineTokenizer() {
                  {
                      setNames(new String[] {"SC_CODE", "SC_NAME", "SC_GROUP", "SC_TYPE", "OPEN", "HIGH", "LOW",
                              "CLOSE", "LAST", "PREVCLOSE", "NO_TRADES", "NO_OF_SHRS", "NET_TURNOV", "TDCLOINDI"});
                  }
              });
              setFieldSetMapper(new BeanWrapperFieldSetMapper<BSEBhavCopyVO>() {
                  {
                      setTargetType(BSEBhavCopyVO.class);
                  }
              });
          }
      });
      return reader;
  }
    
    @Lazy
    @Bean
    public Step syncNseBhavCopyStep() {
        return stepBuilderFactory.get("syncNseBhavCopyStep")
                .<NSEBhavCopyVO, BhavCopyVO>chunk(1000)
                .reader(nseBhavCopyReader())
                .processor(nseBhavCopyItemProcessor)
                .writer(writer())
                .build();
    }
    
    public FlatFileItemReader<NSEBhavCopyVO> nseBhavCopyReader() {
      final FlatFileItemReader<NSEBhavCopyVO> reader = new FlatFileItemReader<>();
      reader.setResource(bhavCopyReader.reader(bhavCopyReaderHelper.getNseBhavCopyDownloadUrl()));
      reader.setLinesToSkip(1);
      reader.setLineMapper(new DefaultLineMapper<NSEBhavCopyVO>() {
          {
              setLineTokenizer(new DelimitedLineTokenizer() {
                  {
                      setNames(new String[] {"SYMBOL", "SERIES", "OPEN", "HIGH", "LOW", "CLOSE", "LAST", "PREVCLOSE",
                              "TOTTRDQTY", "TOTTRDVAL", "TIMESTAMP", "TOTALTRADES", "ISIN", "DUMMY"});
                  }
              });
              setFieldSetMapper(new BeanWrapperFieldSetMapper<NSEBhavCopyVO>() {
                  {
                      setTargetType(NSEBhavCopyVO.class);
                  }
              });
          }
      });
      return reader;
  }
    
    @Lazy
    @Bean
    public Step updateCompanySharePriceStep() {
        return stepBuilderFactory.get("updateCompanySharePriceStep")
                .<BhavCopyVO, BhavCopyVO>chunk(1000)
                .reader(bhavCopyItemReader)
                .processor(bhavCopyItemProcessor)
                .writer(updateCompanyWriter())
                .build();
    }
    
    public Step sortCompanyTableStep() {
      return stepBuilderFactory.get("sortCompanyTableStep")
              .tasklet(sortCompanyTableTasklet)
              .build();
    }
    
    
}
