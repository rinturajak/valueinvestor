package com.valueinvestor.batch.item.excel.mapping;

import java.util.Arrays;

import com.valueinvestor.batch.item.excel.RowMapper;
import com.valueinvestor.batch.item.excel.support.rowset.RowSet;
import com.valueinvestor.model.CompanyVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CompanyRowMapper implements RowMapper<CompanyVO> {

    @Override
    public CompanyVO mapRow(final RowSet rs) throws Exception {
        final CompanyVO companyVO = new CompanyVO();
        if (rs.getCurrentRow() != null) {
            log.debug("rs.getCurrentRow() Length ::: "+rs.getCurrentRow().length);
            companyVO.getCellValues().addAll(Arrays.asList(rs.getCurrentRow()));
        }        
        return companyVO;
    }

    
    
}
