package com.valueinvestor.batch.item.excel.mapping;

import java.util.HashMap;
import java.util.Map;

import com.valueinvestor.model.CompanyTableExcelMapperDto;

public class CompanyRowMapperHelper {
  
    public static Map<Integer, CompanyTableExcelMapperDto> excelCompanyMapperMap = new HashMap<>();
    
    /*public static void setBseCode(final CompanyVO companyVO, final String bseCode) {
        companyVO.setBseCode(stripEnd((length(bseCode) >= 6 ? bseCode : null), ".0"));
    }

    public static void setNseCode(final CompanyVO companyVO, final String nseCode) {
        companyVO.setNseSymbol((length(nseCode) > 0) && (nseCode != "0") ? nseCode : null);
    }

    public static void setCompanyName(final CompanyVO companyVO, final String companyName) {
        companyVO.setCompanyName(length(companyName) > 0 ? companyName : null);
    }
    
    public static void setCompanyFullName(final CompanyVO companyVO, final String companyFullName) {
        companyVO.setCompanyFullName(length(companyFullName) > 0 ? companyFullName : null);
    }
    
    public static void setWebsite(final CompanyVO companyVO, final String website) {
        companyVO.setWebsite((length(website) > 0) && (website != "0") ? website : null);
    }
    
    public static void setIndustry(final CompanyVO companyVO, final String industry) {
        companyVO.setIndustry(length(industry) > 0 ? industry : null);
    }
    
    public static void setTotalPromoterGroup(final CompanyVO companyVO, final String totalPromoterGroup) {
        companyVO.setTotalPromoterGroup(getDoubleValue(totalPromoterGroup));
    }

    public static void setGrandTotal(final CompanyVO companyVO, final String grandTotal) {
        companyVO.setGrandTotal(length(grandTotal) > 0 ? Double.valueOf(grandTotal).longValue() : 0);
    }
    
    public static void setEpsUnit(final CompanyVO companyVO, final String epsUnit) {
        companyVO.setEpsUnit(getDoubleValue(epsUnit));
    }
    
    public static void setDivPayout(final CompanyVO companyVO, final String divPayout) {
        companyVO.setDivPayout(getDoubleValue(divPayout));
    }
    
    public static void setFiveDivPayout(final CompanyVO companyVO, final String fiveDivPayout) {
        companyVO.setFiveDivPayout(getDoubleValue(fiveDivPayout));
    }
    
    public static void setTrailingEbit(final CompanyVO companyVO, final String trailingEbit) {
        companyVO.setTrailingEbit(length(trailingEbit) > 0 ? Double.valueOf(trailingEbit).intValue() : 0);
    }
    
    public static void setTrailingSales(final CompanyVO companyVO, final String trailingSales) {
        companyVO.setTrailingSales(getDoubleValue(trailingSales));
    }
    
    public static void setTotalDebt(final CompanyVO companyVO, final String totalDebt) {
        companyVO.setTotalDebt(getDoubleValue(totalDebt));
    }
    
    public static void setCashEv(final CompanyVO companyVO, final String cashEv) {
        companyVO.setCashEv(getDoubleValue(cashEv));
    }
    
    public static void setPercentageDe(final CompanyVO companyVO, final String percentageDe) {
        companyVO.setPercentageDe(getCompanyBigDecimal(percentageDe));
    }
    
    public static void setPercentageEpsThreeYears(final CompanyVO companyVO, final String percentageEpsThreeYears) {
        companyVO.setPercentageEpsThreeYears(getCompanyBigDecimal(percentageEpsThreeYears));
    }
    
    public static void setPercentageEpsFiveYears(final CompanyVO companyVO, final String percentageEpsFiveYears) {
        companyVO.setPercentageEpsFiveYears(getCompanyBigDecimal(percentageEpsFiveYears));
    }
    
    public static void setPercentageRogcOne(final CompanyVO companyVO, final String percentageRogcOne) {
        companyVO.setPercentageRogcOne(getCompanyBigDecimal(percentageRogcOne));
    }
    
    public static void setPercentageRonc(final CompanyVO companyVO, final String percentageRonc) {
        companyVO.setPercentageRonc(getCompanyBigDecimal(percentageRonc));
    }
    
    public static void setPercentageModRoncFive(final CompanyVO companyVO, final String percentageModRoncFive) {
        companyVO.setPercentageModRoncFive(getCompanyBigDecimal(percentageModRoncFive));
    }
    
    public static void setPercentageRogcFive(final CompanyVO companyVO, final String percentageRogcFive) {
        companyVO.setPercentageRogcFive(getCompanyBigDecimal(percentageRogcFive));
    }
    
    public static void setSharePrice(final CompanyVO companyVO, final String sharePrice) {
        companyVO.setSharePrice(Double.parseDouble(sharePrice));
    }
    
    public static void setCreationDate(final CompanyVO companyVO) {
        companyVO.setCreationDate(new Date());
    }
    
    public static void setModifiedDate(final CompanyVO companyVO) {
        companyVO.setModifiedDate(new Date());
    }*/
    
    
}
