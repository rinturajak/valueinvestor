/*
 * Copyright 2006-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.valueinvestor.batch.item.excel.poi;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

import com.valueinvestor.batch.item.excel.Sheet;

import lombok.extern.slf4j.Slf4j;

/**
 * Sheet implementation for Apache POI.
 *
 * @author Marten Deinum
 * @since 0.5.0
 */
@Slf4j
public class PoiSheet implements Sheet {

    private final org.apache.poi.ss.usermodel.Sheet delegate;
    private final int numberOfRows;
    private final String name;

    private int numberOfColumns = -1;
    private FormulaEvaluator evaluator;

    PoiSheet(final org.apache.poi.ss.usermodel.Sheet delegate) {
        super();
        this.delegate = delegate;
        this.numberOfRows = this.delegate.getLastRowNum() + 1;
        this.name=this.delegate.getSheetName();
    }

    @Override
    public int getNumberOfRows() {
        return this.numberOfRows;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String[] getRow(final int rowNumber) {
        final Row row = this.delegate.getRow(rowNumber);
        if (row == null) {
            return null;
        }
        final List<String> cells = new LinkedList<>();

        for (int i = 0; i < getNumberOfColumns(); i++) {
            final Cell cell = row.getCell(i);
            switch (cell.getCellTypeEnum()) {
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        final Date date = cell.getDateCellValue();
                        cells.add(String.valueOf(date.getTime()));
                    } else {
                        cells.add(String.valueOf(cell.getNumericCellValue()));
                    }
                    log.debug("Cell =========> NUMERIC "+cell + " position = "+i);
                    break;
                case BOOLEAN:
                    log.debug("Cell =========> BOOLEAN "+cell + " position = "+i);
                    cells.add(String.valueOf(cell.getBooleanCellValue()));
                    break;
                case STRING:
                case BLANK:
                    log.debug("Cell =========> BLANK "+cell + " position = "+i);
                    cells.add(cell.getStringCellValue());
                    break;
                case FORMULA:
                    log.debug("Cell =========> Formula "+cell + " position = "+i);
                    cells.add(getFormulaEvaluator().evaluate(cell).formatAsString());
                    break;
                case ERROR:
                    log.debug("Cell =========> ERROR "+cell + " position = "+i);
                    cells.add("");
                    break;
                default:
                    log.debug("Cell =========> Default " + cell + " position = "+i);
                    //throw new IllegalArgumentException("Cannot handle cells of type " + cell.getCellTypeEnum());
            }
        }
        return cells.toArray(new String[cells.size()]);
    }

    private FormulaEvaluator getFormulaEvaluator() {
        if (this.evaluator == null) {
            this.evaluator = delegate.getWorkbook().getCreationHelper().createFormulaEvaluator();
        }
        return this.evaluator;
    }

    @Override
    public int getNumberOfColumns() {
        if (numberOfColumns < 0) {
            numberOfColumns = this.delegate.getRow(0).getLastCellNum();
        }
        return numberOfColumns;
    }
}
