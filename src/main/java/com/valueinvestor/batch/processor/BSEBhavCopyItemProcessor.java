package com.valueinvestor.batch.processor;

import java.util.Date;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.valueinvestor.model.BSEBhavCopyVO;
import com.valueinvestor.model.BhavCopyVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class BSEBhavCopyItemProcessor implements ItemProcessor<BSEBhavCopyVO, BhavCopyVO> {

    @Override
    public BhavCopyVO process(final BSEBhavCopyVO bseBhavCopyVO) throws Exception {
        log.debug("bseBhavCopyVO: " + new ObjectMapper().writeValueAsString(bseBhavCopyVO));
        final BhavCopyVO bhavCopyVO = new BhavCopyVO();
        bhavCopyVO.setCode(bseBhavCopyVO.getScCode());
        bhavCopyVO.setName(bseBhavCopyVO.getScName());
        bhavCopyVO.setClosingPrice(bseBhavCopyVO.getClose());
        bhavCopyVO.setModifiedDate(new Date());
        log.debug("bhavCopy: " + new ObjectMapper().writeValueAsString(bhavCopyVO));
        return bhavCopyVO;
    }

}
