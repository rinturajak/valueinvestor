package com.valueinvestor.batch.processor;

import java.util.Date;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.valueinvestor.model.BhavCopyVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class BhavCopyItemProcessor implements ItemProcessor<BhavCopyVO, BhavCopyVO> {

    @Override
    public BhavCopyVO process(final BhavCopyVO bhavCopyVO) throws Exception {
        bhavCopyVO.setModifiedDate(new Date());
        return bhavCopyVO;
    }

}
