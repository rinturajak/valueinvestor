package com.valueinvestor.batch.processor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.valueinvestor.batch.tasklet.PopulateExcelMapperTasklet;
import com.valueinvestor.model.CompanySQLDto;
import com.valueinvestor.model.CompanyTableExcelMapperDto;
import com.valueinvestor.model.CompanyVO;
import com.valueinvestor.util.ValueInvestorUtil;

import lombok.extern.slf4j.Slf4j;

@Lazy
@Slf4j
@Component
public class CompanyItemProcessor implements ItemProcessor<CompanyVO, CompanySQLDto> {

  @Resource
  private PopulateExcelMapperTasklet populateExcelMapperTasklet;

  @Override
  public CompanySQLDto process(final CompanyVO companyVO) throws Exception {
    final CompanySQLDto companySQLDto = new CompanySQLDto();
    if (companyVO.getCellValues().size() > 5 ) {
      final String sql = getInsertSql(companySQLDto, companyVO);
      companySQLDto.setInsertSql(sql);
      return companySQLDto;
    }
    return null;
  }

  public String getInsertSql(final CompanySQLDto companySQLDto, final CompanyVO companyVO) {
    final StringBuilder insertSQLStringBuilder = new StringBuilder();
    final StringBuilder columnStringBuilder = new StringBuilder();
    final StringBuilder valueStringBuilder = new StringBuilder();
    final List<String> cellValues = companyVO.getCellValues();
    final Map<Integer, CompanyTableExcelMapperDto> excelCompanyMapperMap = populateExcelMapperTasklet.getExcelCompanyMapperMap();
    
    for (final Map.Entry<Integer, CompanyTableExcelMapperDto> excelMapper : excelCompanyMapperMap.entrySet()) {
      final CompanyTableExcelMapperDto excelMapperDto = excelMapper.getValue();
      columnStringBuilder.append(excelMapperDto.getDbColumnName());
      columnStringBuilder.append(",");
      
      valueStringBuilder.append("\"");
      valueStringBuilder.append(getProperCellValue(cellValues.get(excelMapperDto.getExcelCellPosition() - 1), excelMapperDto.getDbColumnType()));
      valueStringBuilder.append("\",");
    }
    
    final String timestamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(Calendar.getInstance().getTime());
    
    insertSQLStringBuilder.append("INSERT INTO company_table ( ");
    insertSQLStringBuilder.append(columnStringBuilder.toString());
    insertSQLStringBuilder.append("creation_date, modified_date");
    insertSQLStringBuilder.append(" ) VALUES ( ");
    insertSQLStringBuilder.append(valueStringBuilder.toString());
    insertSQLStringBuilder.append("\""+timestamp+"\"");
    insertSQLStringBuilder.append(",");
    insertSQLStringBuilder.append("\""+timestamp+"\"");
    insertSQLStringBuilder.append(" )");
    
    return insertSQLStringBuilder.toString();

  }

  public String getProperCellValue(final String cellValue, final String dataType) {
    String returnedValue = "";
    log.debug("datatype = "+dataType);
    //TODO - Change it to Enum
    switch (dataType) {
      case "varchar":
        returnedValue = ValueInvestorUtil.getCompanyStringValue(cellValue);
        break;
      case "decimal":
        returnedValue = ValueInvestorUtil.getCompanyDecimalValue(cellValue);
        break;
      case "double":
        returnedValue = ValueInvestorUtil.getCompanyDoubleValue(cellValue);
        break;
      case "tinyint":
        returnedValue = ValueInvestorUtil.getCompanyTinyIntValue(cellValue);
        break;
      default:
        returnedValue = cellValue;
    }
    return returnedValue;
  }

}
