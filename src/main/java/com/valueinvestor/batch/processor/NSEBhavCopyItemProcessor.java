package com.valueinvestor.batch.processor;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.valueinvestor.load.LoadNseCode;
import com.valueinvestor.model.BhavCopyVO;
import com.valueinvestor.model.NSEBhavCopyVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class NSEBhavCopyItemProcessor implements ItemProcessor<NSEBhavCopyVO, BhavCopyVO> {

    @Resource
    private LoadNseCode loadNseCode;

    @Override
    public BhavCopyVO process(final NSEBhavCopyVO nseBhavCopyVO) throws Exception {
        if (isNseCodeValid(nseBhavCopyVO)) {
            final BhavCopyVO bhavCopyVO = new BhavCopyVO();
            bhavCopyVO.setCode(nseBhavCopyVO.getSymbol());
            bhavCopyVO.setName(loadNseCode.getNseCodesMap().get(nseBhavCopyVO.getSymbol().toLowerCase()));
            bhavCopyVO.setClosingPrice(nseBhavCopyVO.getClose());
            bhavCopyVO.setModifiedDate(new Date());
            return bhavCopyVO;
        } else {
            log.debug("Ignoring the code as it is not present in nseCode csv: " + nseBhavCopyVO.getSymbol()
                    + " and series: " + nseBhavCopyVO.getSeries());
        }
        return null;
    }

    private boolean isNseCodeValid(final NSEBhavCopyVO nseBhavCopyVO) {
        return loadNseCode.getNseCodesMap().containsKey(nseBhavCopyVO.getSymbol().toLowerCase())
                && nseBhavCopyVO.getSeries().equalsIgnoreCase("EQ");
    }

}
