package com.valueinvestor.batch.reader;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.valueinvestor.model.BhavCopyVO;

@Lazy
@Configuration
public class BhavCopyReaderConfiguration {
    
    @Resource
    private DataSource dataSource;
    
    @Lazy
    @Bean
    public ItemReader<BhavCopyVO> bhavCopyItemReader() {
        final JdbcCursorItemReader<BhavCopyVO> databaseReader = new JdbcCursorItemReader<>(); 
        databaseReader.setDataSource(dataSource);
        databaseReader.setSql("SELECT bhav_copy.code as code, bhav_copy.closing_price as closingPrice FROM valueinvestor.company_table company, valueinvestor.bhav_copy bhav_copy WHERE company.bse_code = bhav_copy.code OR company.nse_symbol = bhav_copy.code");
        databaseReader.setRowMapper(new BeanPropertyRowMapper<>(BhavCopyVO.class));
        return databaseReader;
    }

}
