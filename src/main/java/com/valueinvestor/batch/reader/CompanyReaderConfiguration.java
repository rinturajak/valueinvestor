package com.valueinvestor.batch.reader;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;

import com.valueinvestor.batch.item.excel.RowMapper;
import com.valueinvestor.batch.item.excel.mapping.CompanyRowMapper;
import com.valueinvestor.batch.item.excel.poi.PoiItemReader;
import com.valueinvestor.model.CompanyVO;

@Lazy
@Configuration
public class CompanyReaderConfiguration {
    
    public PoiItemReader<CompanyVO> companyReader(final Resource resource) {
        final PoiItemReader<CompanyVO> reader = new PoiItemReader<>();
        reader.setResource(resource);
        reader.setLinesToSkip(1);
        reader.setRowMapper(excelRowMapper());
        return reader;
    }
    
    private RowMapper<CompanyVO> excelRowMapper() {
        return new CompanyRowMapper();
     }

}
