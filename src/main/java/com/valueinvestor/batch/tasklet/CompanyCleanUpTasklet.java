package com.valueinvestor.batch.tasklet;

import javax.annotation.Resource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class CompanyCleanUpTasklet implements Tasklet {
    
    @Resource
    private JdbcTemplate jdbcTemplate;

    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext) throws Exception {
        jdbcTemplate.execute("DELETE FROM company_table");
        return RepeatStatus.FINISHED;
    }

}
