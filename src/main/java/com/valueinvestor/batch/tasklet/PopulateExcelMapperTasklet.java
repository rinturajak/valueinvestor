package com.valueinvestor.batch.tasklet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.valueinvestor.model.CompanyTableExcelMapperDto;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Lazy
@Slf4j
@Component
public class PopulateExcelMapperTasklet implements Tasklet {

  private static final String EXCEL_MAPPER_SQL =
      "SELECT table_column.column_position, table_column.column_name, table_column.column_type, table_column.column_label, excel_cell.cell_position, excel_cell.cell_label, excel_cell.custom_label FROM company_table_column table_column, company_excel_cell excel_cell, company_excel_table_mapper mapper WHERE mapper.column_position = table_column.column_position AND  mapper.cell_position = excel_cell.cell_position ORDER BY mapper.cell_position ASC";

  @Resource
  private JdbcTemplate jdbcTemplate;
  
  @Getter
  public Map<Integer, CompanyTableExcelMapperDto> excelCompanyMapperMap = new HashMap<>();

  @Override
  public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext)
      throws Exception {
    getExcelCompanyMapperMap().clear();
    jdbcTemplate.query(EXCEL_MAPPER_SQL, new CompanyExcelRowMapper());
    return RepeatStatus.FINISHED;
  }
  
  private class CompanyExcelRowMapper implements RowMapper<CompanyTableExcelMapperDto> {

    @Override
    public CompanyTableExcelMapperDto mapRow(final ResultSet resultSet, final int count) throws SQLException {
      final CompanyTableExcelMapperDto excelMapperDto = new CompanyTableExcelMapperDto();
      excelMapperDto.setDbColumnPosition(resultSet.getInt("column_position"));
      excelMapperDto.setDbColumnName(resultSet.getString("column_name"));
      excelMapperDto.setDbColumnType(resultSet.getString("column_type"));
      excelMapperDto.setDbColumnLabel(resultSet.getString("column_label"));
      
      excelMapperDto.setExcelCellPosition(resultSet.getInt("cell_position"));
      excelMapperDto.setExcelCellLabel(resultSet.getString("cell_label"));
      excelMapperDto.setExcelCustomLabel(resultSet.getString("custom_label"));
      
      
      getExcelCompanyMapperMap().put(resultSet.getInt("cell_position"), excelMapperDto);
      log.debug("getExcelCompanyMapperMap()  "+getExcelCompanyMapperMap());
      return excelMapperDto;
    }
  }

}
