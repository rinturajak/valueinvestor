package com.valueinvestor.batch.tasklet;

import javax.annotation.Resource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class SortCompanyTableTasklet implements Tasklet {
    
    @Resource
    private JdbcTemplate jdbcTemplate;

    @Override
    public RepeatStatus execute(final StepContribution contribution, final ChunkContext chunkContext) throws Exception {
        log.debug("Executing sort_company_table store procedure");
        jdbcTemplate.execute("CALL sort_company_table()");
        return RepeatStatus.FINISHED;
    }

}
