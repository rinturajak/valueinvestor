package com.valueinvestor.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.valueinvestor.batch.config.BatchConfiguration;

@Configuration
@EnableWebMvc
@ComponentScan({"com.valueinvestor"})
@Import({BatchConfiguration.class})
@PropertySource("classpath:application.properties")
public class ValueInvestorConfig extends WebMvcConfigurerAdapter  {  
    
    private final int maxUploadSizeInMb = 5 * 1024 * 1024; //5 MB
    
    @Bean  
    public InternalResourceViewResolver viewResolver() {  
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();  
        resolver.setPrefix("/WEB-INF/view/");  
        resolver.setSuffix(".jsp");
        return resolver;  
    }
    
    @Bean
    public CommonsMultipartResolver multipartResolver() {

        final CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setMaxUploadSize(maxUploadSizeInMb * 2);
        commonsMultipartResolver.setMaxUploadSizePerFile(maxUploadSizeInMb); //bytes
        return commonsMultipartResolver;

    }
    
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }  
}
