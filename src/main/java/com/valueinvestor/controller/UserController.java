package com.valueinvestor.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.valueinvestor.auth.service.JwtTokenService;
import com.valueinvestor.auth.service.SecurityService;
import com.valueinvestor.auth.service.UserService;
import com.valueinvestor.jpa.auth.User;
import com.valueinvestor.security.model.AuthenticatedUser;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class UserController {
    
    @Resource
    private UserService userService;
    
    @Resource
    private SecurityService securityService;
    
    @Resource
    private JwtTokenService jwtTokenService;

    @GetMapping("/login")
    public ModelAndView login() {
        return new ModelAndView ("login");
    }
    
    @PostMapping("/login")
    public ResponseEntity<AuthenticatedUser> login(final User user) {
        String jwtToken = null;
        final AuthenticatedUser authenticatedUser = new AuthenticatedUser();
        securityService.generateAndSetUsernameAuthenticationToken(user.getUsername(), user.getPassword());
        if (SecurityContextHolder.getContext().getAuthentication() != null ) {
            final HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                    .getResponse();
            jwtToken  = jwtTokenService.addAuthentication(user, response);
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            final org.springframework.security.core.userdetails.User user1 = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
            authenticatedUser.setUsername(user1.getUsername());
            authenticatedUser.setToken(jwtToken);
        }
        
        return new ResponseEntity<>(authenticatedUser, HttpStatus.OK);
    }
    
    @GetMapping("/registration")
    public ModelAndView registration() {
        return new ModelAndView ("registration");
    }
    
    @PostMapping("/registration")
    public ResponseEntity<User> registration(final User user) {
        userService.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
    
    
    
}
