package com.valueinvestor.controller;

import java.security.Principal;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Resource;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.valueinvestor.batch.company.CompanySyncJob;
import com.valueinvestor.model.Greeting;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ValueInvestorController {

    @Resource
    private CompanySyncJob companySyncJob;
    
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();


    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") final String name) {

        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }
    
    @RequestMapping(value = "/hello1", method = RequestMethod.GET)
    public Greeting greeting1(@RequestParam(value = "name", defaultValue = "World") final String name) {

        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @RequestMapping("/")
    public ModelAndView home() {
        return new ModelAndView ("index");
    }
    
    @GetMapping("/upload")
    public ModelAndView upload() {
        return  new ModelAndView ("upload");
    }
    
    @PostMapping("/upload")
    public ModelAndView singleFileUpload(@RequestParam("file") final MultipartFile file,
            final Model model) throws Exception {
        log.debug("I am inside file upload");
     // Save file on system
        if (!file.getOriginalFilename().isEmpty()) {
            final byte[] bytes = file.getBytes();
            companySyncJob.executeCompanySyncJob(new ByteArrayResource(bytes));

           model.addAttribute("msg", "File uploaded successfully.");
        } else {
           model.addAttribute("msg", "Please select a valid file..");
        }

        return new ModelAndView ("upload");
    }
    
    @RequestMapping(value = {"/user", "/me"}, method = RequestMethod.POST)
    public ResponseEntity<?> user(final Principal principal) {
        log.debug("Inside User ======================");
        log.debug("Principal ======== "+principal);
        return ResponseEntity.ok(principal);
    }
    
}
