package com.valueinvestor.load;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Component
public class LoadNseCode {

    public static final String NSE_CODE_FILE = "NseCode.csv";

    private final Map<String, String> nseCodesMap = new HashMap<>();

    @PostConstruct
    public void loadNseCodes() throws URISyntaxException {
        final Path nseCodePath = Paths.get(getClass().getClassLoader().getResource(NSE_CODE_FILE).toURI());
        
        try (final BufferedReader bufferedReader = Files.newBufferedReader(nseCodePath)) {
            String nseRecords = null;
            while ((nseRecords = bufferedReader.readLine()) != null) {
                final String[] nseRecord = nseRecords.split(",");
                nseCodesMap.put(nseRecord[1].toLowerCase(), nseRecord[0]);
            }
        } catch (final IOException exception) {
            log.error("Exception while loading the NSE code: " + exception.getMessage());
        }
    }

    public static void main(final String[] args) throws Exception {
        final LoadNseCode loadNseCode = new LoadNseCode();
        loadNseCode.loadNseCodes();
        log.debug(loadNseCode.getNseCodesMap().toString());
    }

}
