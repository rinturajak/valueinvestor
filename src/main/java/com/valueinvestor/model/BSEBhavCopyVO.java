package com.valueinvestor.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(content = Include.NON_NULL)
public class BSEBhavCopyVO {
    
    @JsonProperty("SC_CODE")
    private String scCode;
    
    @JsonProperty("SC_NAME")
    private String scName;
    
    @JsonProperty("SC_GROUP")
    private String scGroup;
    
    @JsonProperty("SC_TYPE")
    private String scType;
    
    @JsonProperty("OPEN")
    private double open;
    
    @JsonProperty("HIGH")
    private double high;
    
    @JsonProperty("LOW")
    private double low;
    
    @JsonProperty("CLOSE")
    private double close;
    
    @JsonProperty("LAST")
    private double last;
    
    @JsonProperty("PREVCLOSE")
    private double prevClose;
    
    @JsonProperty("NO_TRADES")
    private int noTrades;
    
    @JsonProperty("NO_OF_SHRS")
    private int noOfShares;
    
    @JsonProperty("NET_TURNOV")
    private double netTurnOver;
    
    @JsonProperty("TDCLOINDI")
    private String tdcCloIndi;
    
}
