package com.valueinvestor.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BhavCopyVO {

    private int id;

    private String code;

    private String name;

    private double closingPrice;

    private Date modifiedDate;

}
