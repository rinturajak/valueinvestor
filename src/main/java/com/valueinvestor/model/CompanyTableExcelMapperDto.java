package com.valueinvestor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyTableExcelMapperDto {
  
  private int dbColumnPosition;
  private int dbColumnPositionOld;  
  private String dbColumnName;
  private String dbColumnType;
  private String dbColumnLabel;
  
  private int excelCellPosition;
  private int excelCellPositionOld;
  private String excelCellLabel;
  private String excelCustomLabel;
  
}
