package com.valueinvestor.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyVO {
    
  private List<String> cellValues = new ArrayList<>();

}
