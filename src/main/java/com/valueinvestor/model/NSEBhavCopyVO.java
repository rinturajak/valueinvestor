package com.valueinvestor.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(content = Include.NON_NULL)
public class NSEBhavCopyVO {
    
    @JsonProperty("SYMBOL")
    private String symbol;
    
    @JsonProperty("SERIES")
    private String series;
    
    @JsonProperty("OPEN")
    private double open;
    
    @JsonProperty("HIGH")
    private double high;
    
    @JsonProperty("LOW")
    private double low;
    
    @JsonProperty("CLOSE")
    private double close;
    
    @JsonProperty("LAST")
    private double last;
    
    @JsonProperty("PREVCLOSE")
    private double prevClose;
    
    @JsonProperty("TOTTRDQTY")
    private int totTrdQty;
    
    @JsonProperty("TOTTRDVAL")
    private double totTrdVal;
    
    @JsonProperty("TIMESTAMP")
    private String timestamp;
    
    @JsonProperty("TOTALTRADES")
    private int totalTrades;
    
    @JsonProperty("ISIN")
    private String isin;
    
    @JsonProperty("DUMMY")
    private String dummy;
    
}
