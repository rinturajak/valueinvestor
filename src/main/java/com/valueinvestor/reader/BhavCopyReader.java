package com.valueinvestor.reader;

import static org.apache.commons.io.IOUtils.toByteArray;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class BhavCopyReader {

    public Resource reader(final String urlString) {
        log.debug("#fetching records from the url: " + urlString);
        InputStream openStream = null;
        ZipInputStream zipInputStream = null;
        try {
            final URL url = new URL(urlString);
            openStream = url.openStream();

            zipInputStream = new ZipInputStream(openStream);
            ZipEntry entry = null;
            if ((entry = zipInputStream.getNextEntry()) != null) {
                log.debug("File found inside the given zip: " + entry.getName());
                return new ByteArrayResource(toByteArray(zipInputStream));
            }
        } catch (final IOException exception) {
            log.error("Exception while reading the file from the url: " + urlString, exception);
        } finally {
            try {
                zipInputStream.closeEntry();
                zipInputStream.close();
            } catch (final IOException ex) {
                log.error("Exception while closing the zip stream", ex);
            }
        }
        log.debug("No File found inside the given zip: " + urlString);
        return null;
    }

}
