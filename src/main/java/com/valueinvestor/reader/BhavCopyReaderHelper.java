package com.valueinvestor.reader;

import static org.apache.commons.lang3.StringUtils.endsWith;
import static org.apache.commons.lang3.StringUtils.remove;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Component
public class BhavCopyReaderHelper {

    private static final String NSE_EQUITIES_PATH = "https://www.nseindia.com/content/historical/EQUITIES/";
    private static final String BSE_EQUITIES_PATH = "http://www.bseindia.com/download/BhavCopy/Equity/";

    public String getNseBhavCopyDownloadUrl() {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMMYYYY");
        final LocalDate today = LocalDate.now();
        final LocalDate yesterday = today.minus(Period.ofDays(1));

        final String todayDate = dtf.format(today).toUpperCase();
        final String yesterdayDate = dtf.format(yesterday).toUpperCase();

        final String month = today.getMonth().toString().substring(0, 3).toUpperCase();

        log.debug("Today Date: " + todayDate);
        log.debug("Yesterday date: " + yesterdayDate);

        final StringBuilder nseUrlBuilder = new StringBuilder();
        nseUrlBuilder.append(NSE_EQUITIES_PATH);
        nseUrlBuilder.append(today.getYear());
        nseUrlBuilder.append("/");
        nseUrlBuilder.append(month);
        nseUrlBuilder.append("/cm");
        nseUrlBuilder.append(yesterdayDate);
        nseUrlBuilder.append("bhav.csv.zip");

        return nseUrlBuilder.toString();
    }
    
    public String getBseBhavCopyDownloadUrl() {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyy");
        final LocalDate today = LocalDate.now();
        final LocalDate yesterday = today.minus(Period.ofDays(1));

        final String todayDate = dtf.format(today);
        final String yesterdayDate = dtf.format(yesterday);

        log.debug("Today Date: " + todayDate);
        log.debug("Yesterday date: " + yesterdayDate);

        final StringBuilder nseUrlBuilder = new StringBuilder();
        nseUrlBuilder.append(BSE_EQUITIES_PATH);
        nseUrlBuilder.append("EQ");
        nseUrlBuilder.append(yesterdayDate);
        nseUrlBuilder.append("_CSV.ZIP");

        return nseUrlBuilder.toString();
    }
    
    public static BigDecimal getCompanyBigDecimal(final String value) {
        return new BigDecimal(endsWith(value, "%") ? remove(value, "%") : "0.00");
    }
}
