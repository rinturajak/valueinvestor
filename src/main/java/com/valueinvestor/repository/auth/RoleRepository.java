package com.valueinvestor.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;

import com.valueinvestor.jpa.auth.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
