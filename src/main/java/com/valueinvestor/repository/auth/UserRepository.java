package com.valueinvestor.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;

import com.valueinvestor.jpa.auth.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
