package com.valueinvestor.security;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.valueinvestor.jpa.auth.Role;
import com.valueinvestor.jpa.auth.User;
import com.valueinvestor.security.exception.JwtTokenMalformedException;
import com.valueinvestor.security.model.AuthenticatedUser;
import com.valueinvestor.security.model.JwtAuthenticationToken;
import com.valueinvestor.security.util.JwtTokenValidator;

/**
 * Used for checking the token from the request and supply the UserDetails if the token is valid
 *
 * @author pascal alma
 */
@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JwtTokenValidator jwtTokenValidator;

    @Override
    public boolean supports(final Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(final UserDetails userDetails, final UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        final JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        final String token = jwtAuthenticationToken.getToken();

        final User parsedUser = jwtTokenValidator.parseToken(token);

        if (parsedUser == null) {
            throw new JwtTokenMalformedException("JWT token is not valid");
        }
        
        final List<GrantedAuthority> authorities = new ArrayList<>();
        
        if (parsedUser.getRoles() != null) {
            for (final Role role : parsedUser.getRoles()) {
                authorities.add(new SimpleGrantedAuthority(role.getName()));
            } 
        }

        return new AuthenticatedUser(parsedUser.getId(), parsedUser.getUsername(), token, authorities);
    }
    
    

}
