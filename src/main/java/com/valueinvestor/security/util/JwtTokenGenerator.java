package com.valueinvestor.security.util;

import java.util.HashSet;
import java.util.Set;

import com.valueinvestor.jpa.auth.Role;
import com.valueinvestor.jpa.auth.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * convenience class to generate a token for testing your requests.
 * Make sure the used secret here matches the on in your application.yml
 *
 * @author pascal alma
 */
public class JwtTokenGenerator {

    /**
     * Generates a JWT token containing username as subject, and userId and role as additional claims. These properties are taken from the specified
     * User object. Tokens validity is infinite.
     *
     * @param u the user for which the token will be generated
     * @return the JWT token
     */
    public static String signAndSerializeJWT(final User user, final String secret) {
        final Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("userId", user.getId() + "");
        claims.put("roles", user.getRoles());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {

        final User user = new User();
        user.setId(123L);
        user.setUsername("Pascal");
        final Set<Role> roles = new HashSet<>();
        final Role role = new Role();
        role.setName("admin");
        roles.add(role);
        user.setRoles(roles);

        System.out.println(signAndSerializeJWT(user, "my-very-secret-key"));
    }
}
