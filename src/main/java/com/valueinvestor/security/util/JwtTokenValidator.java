package com.valueinvestor.security.util;

import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.valueinvestor.jpa.auth.Role;
import com.valueinvestor.jpa.auth.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

@Component
public class JwtTokenValidator {
    
    @Value("${jwt.secret}")
    private String secret;

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param token the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    @SuppressWarnings("unchecked")
    public User parseToken(final String token) {
        User user = null;

        try {
            final Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            
            System.out.println("Rintu Body:: "+body);
            user = new User();
            user.setUsername(body.getSubject());
            user.setUsername((String)  body.get("userId"));
            //user.setId(Long.parseLong((String) body.get("userId")));
            user.setRoles( (Set<Role>) body.get("roles"));

        } catch (final JwtException e) {
            // Simply print the exception and null will be returned for the userDto
            e.printStackTrace();
        }
        return user;
    }
    
}
