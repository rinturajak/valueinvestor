package com.valueinvestor.service;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.valueinvestor.model.BhavCopyVO;

@Lazy
@Repository
public class BhavCopyRepository {
    
    @Resource
    private JdbcTemplate jdbcTemplate;
    
    public int save(final BhavCopyVO bhavCopyVO) {
        return jdbcTemplate.update("INSERT INTO bhav_copy(code, name, closing_price, creation_date, modified_date ) VALUES (?, ?, ?, ?, ?)",
                bhavCopyVO.getCode(), bhavCopyVO.getName(), bhavCopyVO.getClosingPrice(),new Date(), new Date());
    }
}