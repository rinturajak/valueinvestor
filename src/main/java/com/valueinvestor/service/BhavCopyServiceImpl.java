package com.valueinvestor.service;

import javax.annotation.Resource;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.valueinvestor.model.BhavCopyVO;

@Lazy
@Service
public class BhavCopyServiceImpl implements BhavCopyService {
    
    @Resource
    private BhavCopyRepository bhavCopyRepository;
    
    @Override
    public int save(final BhavCopyVO bhavCopyVO) {
        return bhavCopyRepository.save(bhavCopyVO);
    }

}
