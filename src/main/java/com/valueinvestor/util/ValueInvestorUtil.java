package com.valueinvestor.util;

import static org.apache.commons.lang3.StringUtils.endsWith;
import static org.apache.commons.lang3.StringUtils.remove;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ValueInvestorUtil {

  public static BigDecimal getCompanyBigDecimal(final String value) {
    return new BigDecimal(endsWith(value, "%") ? remove(value, "%") : "0.00");
  }

  public static String getCompanyStringValue(final String value) {
    if (StringUtils.endsWith(value, ".0")) {
      return StringUtils.stripEnd(value, ".0");
    }
    
    return value;
  }
  
  public static String getCompanyDoubleValue(final String value) {
    if (StringUtils.isBlank(value) || StringUtils.endsWith(value, "0!") ) {
      return "0.00";
    } 
    final DecimalFormat df2 = new DecimalFormat(".##");
    final Double valueOf = Double.valueOf(value);
    
    return df2.format(valueOf);
  }
  
  public static String getCompanyDecimalValue(final String value) {
    if (endsWith(value, "%")) {
      return remove(value, "%");
    } else if (endsWith(value, "0!") || StringUtils.isBlank(value)) {
      return "0.00";
    }
    return value;
  }
  
  public static String getCompanyTinyIntValue(final String value) {
    if (StringUtils.startsWithAny(value, "1")) {
      return "1";
    }
    return "0";
  }

}
