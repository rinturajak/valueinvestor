CREATE DATABASE valueinvestor;

USE valueinvestor;

DROP TABLE bhav_copy IF EXISTS;

CREATE TABLE bhav_copy 
  ( 
     id            INT NOT NULL auto_increment, 
     code          VARCHAR(32) NOT NULL, 
     name          VARCHAR(128), 
     closing_price DOUBLE, 
     creation_date DATE, 
     modified_date DATE, 
     PRIMARY KEY (id) 
  ); 
	

DROP procedure IF EXISTS bhav_copy_save;

DELIMITER $$

CREATE definer=root@localhost PROCEDURE bhav_copy_save( IN p_code varchar(32), IN p_name varchar(128), IN p_closing_price DOUBLE, IN p_date date)
BEGIN IF EXISTS 
  ( 
         SELECT * 
         FROM   bhav_copy 
         WHERE  code = p_code) then 
  UPDATE bhav_copy 
  SET    closing_price = p_closing_price, 
         modified_date = p_date 
  WHERE  code = p_code; 
   
  else 
  INSERT INTO bhav_copy 
              ( 
                          code, 
                          name, 
                          closing_price, 
                          creation_date, 
                          modified_date 
              ) 
              VALUES 
              ( 
                          p_code, 
                          p_name, 
                          p_closing_price, 
                          p_date, 
                          p_date 
              );
END IF;
END $$

DELIMITER ;

DROP TABLE bhav_copy IF EXISTS;

CREATE TABLE bhav_copy 
  ( 
     id            INT NOT NULL auto_increment, 
     code          VARCHAR(32) NOT NULL, 
     name          VARCHAR(128), 
     closing_price DOUBLE, 
     creation_date DATE, 
     modified_date DATE, 
     PRIMARY KEY (id) 
  ); 

DROP TABLE company_table_column IF EXISTS;

CREATE TABLE company_table_column 
  ( 
     column_position  	INT NOT NULL, 
     column_name      	VARCHAR(128) NOT NULL,
     column_type	    VARCHAR(128) NOT NULL,
     column_label      	VARCHAR(128),
     PRIMARY KEY (column_position)
  ); 
  
DROP TABLE company_excel_cell IF EXISTS;

CREATE TABLE company_excel_cell 
  ( 
     cell_position  	INT NOT NULL, 
     cell_label      	VARCHAR(128),
     custom_label     VARCHAR(128),
     PRIMARY KEY (cell_position)
  ); 


  
DROP TABLE company_excel_table_mapper IF EXISTS;

CREATE TABLE company_excel_table_mapper 
  ( 
     column_position  	INT NOT NULL, 
     cell_position     	INT NOT NULL,
     PRIMARY KEY (column_position),
     FOREIGN KEY (column_position) REFERENCES company_table_column(column_position),
     FOREIGN KEY (cell_position) REFERENCES company_excel_cell(cell_position)
  ); 
  
DROP TABLE company_table IF EXISTS;

CREATE TABLE company_table 
  ( 
     company_name  					varchar(50),
     bse_code      					varchar(32), 
	 nse_symbol    					varchar(32),    
     company_full_name  			varchar(150),
     website    					varchar(150),
     industry    					varchar(150),
     promoter_group_pct	 			double, 
     shares_total 					decimal(20,2),
     payout_latest0			 		decimal(10,2),
     payout_latest1			 		decimal(10,2),
     payout_latest2			 		decimal(10,2),
     payout_latest3			 		decimal(10,2),
     payout_latest4			 		decimal(10,2),     
     book_value						double, 
     eps							double,
     gross_block0					double,
     gross_block1					double,
     gross_block2					double,
     gross_block3					double,
     gross_block4					double,
     sales_trailing					double,
     ebit_trailing					double,
     ebit_margin					decimal(10,2),
     ebit3Yr_margin					decimal(10,2),
     ebit5Yr_margin					decimal(10,2),
     div5Yr_payout					double,
     div_payout						double,
     debt							double,
     cash							double,
     d_e							decimal(10,2), 
     eps1Yr_gwth				  	decimal(10,2),
     eps3Yr_gwth				  	decimal(10,2),
     eps5Yr_gwth				  	decimal(10,2),
     ebit1Yr_gwth				  	decimal(20,2),
     ebit3Yr_gwth				  	decimal(20,2),
     ebit5Yr_gwth				  	decimal(20,2),
     1rogc							decimal(10,2),
     1ronc							decimal(10,2),
     3rogc							decimal(10,2),
     3ronc							decimal(10,2),
     5rogc							decimal(10,2),
     5ronc							decimal(10,2),     
     rogc_1st_Yr					decimal(10,2),
     rogc_2nd_Yr					decimal(10,2),
     rogc_3rd_Yr					decimal(10,2),
     rogc_4th_Yr					decimal(10,2),
     rogc_5th_Yr					decimal(10,2),     
     cashflow						decimal(10,2),
     cashflow2Yr_gwth		   		decimal(10,2),
     cashflow5Yr_gwth		   		decimal(10,2),
     1rogc_cashflow					double,
     1ronc_cashflow					double,
     5rogc_cashflow					double,
     5ronc_cashflow					double,
     cashflow1Yr_margin				double,
     cashflow2Yr_margin				double,
     cashflow3Yr_margin				double,
     cashflow4Yr_margin				double,
     year_end						varchar(10),
     quater_end						varchar(10),
     hide							TINYINT(1),
     share_price					double default 0.0,
     MCap							double(20,0) as (( shares_total * share_price)/10000000),
     ev								decimal(20,0) as (MCap + debt - cash),
     div_yield						double(20,3) as ((div_payout/share_price) * 100),
     pe								double(20,1) as (share_price/eps),
     ev_sales						double(20,3) as (ev/sales_trailing),
     Mcap_sales						double(20,3) as (MCap/sales_trailing),
     ebit_ev						double(20,3) as (ev/ebit_trailing),
     s_ev_sales						int default 0,
     s_mktcap_sales					int default 0,
     s_ebit_ev						int default 0,
     s_ebit3Yr_margin				int default 0,
     s_ebit5Yr_margin				int default 0,     
     s_eps1Yr_gwth					int default 0,
     s_eps3Yr_gwth					int default 0,
     s_eps5Yr_gwth					int default 0,
     s_ebit1Yr_gwth					int default 0,
     s_ebit3Yr_gwth					int default 0,
     s_ebit5Yr_gwth					int default 0,
     s_1rogc						int default 0,
     s_1ronc						int default 0,
     s_3rogc						int default 0,
     s_3ronc						int default 0,
     s_5rogc						int default 0,
     s_5ronc						int default 0,
     mf_gb1							int as (s_1rogc + s_ebit_ev),
     mf_nb1							int as (s_1ronc + s_ebit_ev),
     mf_gb3							int as (s_3rogc + s_ebit_ev),
     mf_nb3							int as (s_3ronc + s_ebit_ev),
     mf_gb5							int as (s_5rogc + s_ebit_ev),
     mf_nb5							int as (s_5ronc + s_ebit_ev),
     mfg_gb1						int as (s_1rogc + s_ebit1Yr_gwth + s_ebit_ev),
     mfg_nb1						int as (s_1ronc + s_ebit1Yr_gwth + s_ebit_ev),
     mfg_gb3						int as (s_3rogc + s_ebit3Yr_gwth + s_ebit_ev),
     mfg_nb3						int as (s_3ronc + s_ebit3Yr_gwth + s_ebit_ev),
     mfg_gb5						int as (s_5rogc + s_ebit5Yr_gwth + s_ebit_ev),
     mfg_nb5						int as (s_5ronc + s_ebit5Yr_gwth + s_ebit_ev),
     mfev_sales1					int as (s_ev_sales + s_1rogc),
     mfev_sales3					int as (s_ev_sales + s_3rogc),
     mfev_sales5					int as (s_ev_sales + s_5rogc),
     mfMCap_sales1					int as (s_mktcap_sales + s_1rogc),
     mfMCap_sales3					int as (s_mktcap_sales + s_3rogc),
     mfMCap_sales5					int as (s_mktcap_sales + s_5rogc),
     
     mfev_sales3_ebitmargin3		int as (s_ev_sales + s_3rogc +  s_ebit3Yr_margin ),
     mfev_sales5_ebitmargin5		int as (s_ev_sales + s_5rogc +  s_ebit5Yr_margin ),
     mfMCap_sales3_ebitmargin3		int as (s_mktcap_sales + s_3ronc +  s_ebit3Yr_margin ),
     mfMCap_sales5_ebitmargin5		int as (s_mktcap_sales + s_5ronc +  s_ebit5Yr_margin ),
     
     s_mf_gb1						int default 0,
     s_mf_nb1						int default 0,
     s_mf_gb3						int default 0,
     s_mf_nb3						int default 0,
     s_mf_gb5						int default 0,
     s_mf_nb5						int default 0,
     s_mfg_gb1						int default 0,
     s_mfg_nb1						int default 0,
     s_mfg_gb3						int default 0,
     s_mfg_nb3						int default 0,
     s_mfg_gb5						int default 0,
     s_mfg_nb5						int default 0,
     s_div_yield					int default 0,
     creation_date 					date, 
     modified_date 					date, 
     id            					int NOT NULL auto_increment, 
     PRIMARY KEY (id) 
  );

  
DROP procedure IF EXISTS 'sort_company_table';

DELIMITER $$
CREATE PROCEDURE 'sort_company_table' ()
BEGIN

SET @counter = 0;

UPDATE 
company_table
SET s_ev_sales = @counter := @counter + 1
ORDER BY ev_sales ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mktcap_sales = @counter := @counter + 1
ORDER BY MCap_sales ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_ebit_ev = @counter := @counter + 1
ORDER BY ebit_ev DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_eps1Yr_gwth = @counter := @counter + 1
ORDER BY eps1Yr_gwth DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_eps3Yr_gwth = @counter := @counter + 1
ORDER BY eps3Yr_gwth DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_eps5Yr_gwth = @counter := @counter + 1
ORDER BY eps5Yr_gwth DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_ebit1Yr_gwth = @counter := @counter + 1
ORDER BY ebit1Yr_gwth DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_ebit3Yr_gwth = @counter := @counter + 1
ORDER BY ebit3Yr_gwth DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_ebit5Yr_gwth = @counter := @counter + 1
ORDER BY ebit5Yr_gwth DESC;

SET @counter = 0;

UPDATE 
company_table
SET s_1rogc = @counter := @counter + 1
ORDER BY 1rogc ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_1ronc = @counter := @counter + 1
ORDER BY 1ronc ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_3rogc = @counter := @counter + 1
ORDER BY 3rogc ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_3ronc = @counter := @counter + 1
ORDER BY 3ronc ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_5rogc = @counter := @counter + 1
ORDER BY 5rogc ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_5ronc = @counter := @counter + 1
ORDER BY 5ronc ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mf_gb1 = @counter := @counter + 1
ORDER BY mf_gb1 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mf_nb1 = @counter := @counter + 1
ORDER BY mf_nb1 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mf_gb3 = @counter := @counter + 1
ORDER BY mf_gb3 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mf_nb3 = @counter := @counter + 1
ORDER BY mf_nb3 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mf_gb5 = @counter := @counter + 1
ORDER BY mf_gb5 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mf_nb5 = @counter := @counter + 1
ORDER BY mf_nb5 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mfg_gb1 = @counter := @counter + 1
ORDER BY mfg_gb1 ASC;


SET @counter = 0;

UPDATE 
company_table
SET s_mfg_nb1 = @counter := @counter + 1
ORDER BY mfg_nb1 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mfg_gb3 = @counter := @counter + 1
ORDER BY mfg_gb3 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mfg_nb3 = @counter := @counter + 1
ORDER BY mfg_nb3 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mfg_gb5 = @counter := @counter + 1
ORDER BY mfg_gb5 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_mfg_nb5 = @counter := @counter + 1
ORDER BY mfg_nb5 ASC;

SET @counter = 0;

UPDATE 
company_table
SET s_div_yield = @counter := @counter + 1
ORDER BY div_yield ASC;

END$$

DELIMITER ;


  

