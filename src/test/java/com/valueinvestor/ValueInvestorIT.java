package com.valueinvestor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.valueinvestor.config.ValueInvestorConfig;
import com.valueinvestor.security.JwtAuthenticationEntryPoint;
import com.valueinvestor.security.config.WebSecurityConfig;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ValueInvestorConfig.class})
@WebAppConfiguration
public class ValueInvestorIT {
    
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void myTest() {
      System.out.println(
          "Done #########%%%%%%%%%%%%%==========> " + applicationContext.getBean(WebSecurityConfig.class));
      
      System.out.println(
              "Done JwtAuthenticationProvider #########%%%%%%%%%%%%%==========> " + applicationContext.getBean(JwtAuthenticationEntryPoint.class));
    }

}
