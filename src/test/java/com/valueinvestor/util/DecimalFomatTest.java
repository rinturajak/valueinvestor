package com.valueinvestor.util;

import java.text.DecimalFormat;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DecimalFomatTest {
  
  @Test
  public void testDecimalFormat() {
    final DecimalFormat df2 = new DecimalFormat(".##");
    final String value1 =  "371.34";
    final String value2 =  "-674784371.9999999999";
    
    log.debug("trying to format: "+value1);    
    final Double valueOf = Double.valueOf(value1);
    
    System.out.println(df2.format(valueOf));
    
    final Double valueOf2 = Double.valueOf(value2);
    System.out.println(df2.format(valueOf2));
  }

}
